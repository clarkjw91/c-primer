#include "bintree.h"
#include "inode.h"
#include <iostream>
#include <assert.h>

int BinTree::mCount = 0;
BinTree* (BinTree::*ptrToMove)(eBranchSel) const = &BinTree::movePtrDown;


BinTree::BinTree(int val){
  cNode = new INode(val);
  assert(cNode != 0);
  mCount++;
}

BinTree::BinTree(const BinTree* const p){
  assert(p != nullptr);
  cNode = new INode(p->getValue());
  if(p->movePtrDown(LEFT) != nullptr){
    cNode->mVLeft = new BinTree((p->*ptrToMove)(LEFT));
    assert(cNode->mVLeft != 0);
  }
  if(p->movePtrDown(RIGHT) != nullptr){
    cNode->mVRight = new BinTree((p->*ptrToMove)(RIGHT));
    assert(cNode->mVRight != 0);
  }
mCount++;
}

BinTree::BinTree(const BinTree& c){
  cNode = new INode(c.getValue());
  assert(cNode != 0);
  if((c.*ptrToMove)(LEFT) != nullptr){
    cNode->mVLeft = new BinTree(*(c.*ptrToMove)(LEFT));
    assert(cNode->mVLeft != 0);
  }
  if((c.*ptrToMove)(RIGHT) != nullptr){
    cNode->mVRight = new BinTree(*(c.*ptrToMove)(RIGHT));
    assert(cNode->mVRight != 0);
  }
mCount++;

}

BinTree::~BinTree(){
  if(cNode->mVLeft != nullptr)
    cNode->mVLeft->~BinTree();
  if (cNode->mVRight != nullptr)
    cNode->mVRight->~BinTree();

  delete cNode;
}

void BinTree::insertValue(int x){
  if(x < cNode->mVal){
    if(cNode->mVLeft != nullptr)
      cNode->mVLeft->insertValue(x);
    else
      cNode->mVLeft = new BinTree(x);
      assert(cNode->mVLeft != 0);
  }
  else{
    if(cNode->mVRight != nullptr)
      cNode->mVRight->insertValue(x);
    else
      cNode->mVRight = new BinTree(x);
      assert(cNode->mVRight != 0);
  }
  mCount = 0;
}


void BinTree::printTree() const {
  std::cout << "\n current Value: " << cNode->mVal;
  if(cNode->mVLeft != nullptr)
      cNode->mVLeft->printTree();
  if(cNode->mVRight != nullptr)
      cNode->mVRight->printTree();
}

int BinTree::getValue() const {
  return cNode->mVal;
}


BinTree* BinTree::movePtrDown(eBranchSel s) const {
    switch(s){
      case LEFT:{
      return cNode->mVLeft;
      }
      case RIGHT:{
        return cNode->mVRight;
      }
     }
}

void copyTree(BinTree* dest, BinTree* origin){
  dest->insertValue(origin->getValue());
    if(origin->movePtrDown(LEFT) != nullptr){

      copyTree(dest, (origin->*ptrToMove)(LEFT));
    }
    if(origin->movePtrDown(RIGHT)!= nullptr){
      copyTree(dest, (origin->*ptrToMove)(RIGHT));

    }
}

bool BinTree::operator==(const BinTree& c){
    if(this->getValue() == c.getValue()){
      if(this->movePtrDown(LEFT) != nullptr && c.movePtrDown(LEFT) != nullptr)
        *(this->movePtrDown(LEFT)) == *(c.movePtrDown(LEFT));
      if(this->movePtrDown(RIGHT) != nullptr && c.movePtrDown(RIGHT) != nullptr)
        *(this->movePtrDown(RIGHT)) == *(c.movePtrDown(RIGHT));
      return true;
    }else {
      return false;
    }
}

bool BinTree::operator!=(const BinTree& c){
    if(this->getValue() != c.getValue()){
      if(this->movePtrDown(LEFT) != nullptr)
        *(this->movePtrDown(LEFT)) != *(c.movePtrDown(LEFT));
      if(this->movePtrDown(RIGHT) != nullptr)
          *(this->movePtrDown(RIGHT)) != *(c.movePtrDown(RIGHT));
    }
}
