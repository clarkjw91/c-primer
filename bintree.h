#ifndef BINTREE_H
#define BINTREE_H

class INode;

enum eBranchSel {LEFT, RIGHT};

class BinTree{
public:
  static int mCount;
  BinTree(int);
  BinTree(const BinTree* const );
  BinTree(const BinTree&);
  ~BinTree();
  void insertValue(int);
//getter const functions
void printTree() const;
  int getValue() const;
  BinTree* movePtrDown(eBranchSel) const;


//NYI
friend void copyTree(BinTree*, BinTree*);
bool operator==(const BinTree&);
bool operator!=(const BinTree&);

private:
  INode * cNode;
};

 #endif //BINTREE_H

//Notes
//Copy tree Done
//insertNode Done

//!= DOne
//== Done
