//#include <iostream>

#include "inode.h"

INode::INode(int v){
    mVal = v;
    mVLeft = nullptr;
    mVRight = nullptr;
}
