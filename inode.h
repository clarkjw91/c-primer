#ifndef INODE_H
#define INODE_H

class BinTree;

class INode{
private:
  INode(int);
  friend class BinTree;
  int mVal;
  BinTree* mVLeft;
  BinTree* mVRight;
};
#endif //INODE_H
