#include <iostream>

#include "bintree.h"
#include "inode.h"

int main(){
  int sValue = 10;
  int aVal = 10;
BinTree * const root = new BinTree(sValue);
root->insertValue(15);
root->insertValue(10);
root->insertValue(18);

BinTree* n = new BinTree(root);
BinTree* t = root;

BinTree* x = new BinTree(*root);

root->printTree();
std::cout << "\n\n";
n->printTree();
std::cout << "\n\n";
t->printTree();
//std::cout << "Moved Pointer \n";
//curTree->printTree();
//root->printTree();
std::cout << "\n\n";
x->printTree();
if(*x == *root){
  std::cout << "\n These Arrays are the same!";
}else{
  std::cout << "\n These Trees are not the same!";
}
if(*x != *root)
  std::cout << "\n These Arrays not the same!";
else
    std::cout << "\n These Arrays are the same";

root->~BinTree();
n->~BinTree();
t->~BinTree();
x->~BinTree();

//std::cout << curTree;
}
